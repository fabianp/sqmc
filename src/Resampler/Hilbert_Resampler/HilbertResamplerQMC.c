

#include "../../../include/Resampler/Hilbert_Resampler/HilbertResamplerQMC.h"

#include "../../../include/functionsCC.hpp"

	
/*****************************************************************************************************************
			RESAMPLER ALGORITHMS FOR FILTERING
******************************************************************************************************************/

void HilbertResamplerQMC_64(double *quasi, double *parPhi, double *x, int *dx, int *N, double *W, double* xh)
{
	int i,k;
	uint64_t *h=NULL;
	int *J1=(int*)malloc(sizeof(int)*(*N));

	//FILE* fw=NULL;

	if(*dx==1)
	{
		gsl_heapsort_index(J1, x, *N, sizeof(double), compare_double);
	}
	else
	{
		h=(uint64_t*)malloc(sizeof(uint64_t)*(*N));

		HilbertIndex(x, parPhi, dx, N , h);		
	
		gsl_heapsort_index(J1, h, *N, sizeof(uint64_t), compare_uint64);


		/*fw = fopen("hval", "w");
    
     		
     		for(i=0;i<*N;i++)
     		{ 	 
        		fprintf(fw, "%f", (double)(h[J1[i]]));
              		fprintf(fw, "\n ");      
              		
         	 }     
         	 
	
      		fprintf(fw, "),\n nrow=%d)\n\n", *dx);
      		fclose(fw);*/

		free(h);
		h=NULL;
	}

	qsort(quasi, *N, (*dx+1)*sizeof(double), compare_double); 
	
	quasi_Resample(quasi, dx, N, W, J1, x, xh);

	free(J1);
	J1=NULL;
}


void HilbertResamplerQMC_32(double *quasi, double *parPhi, double *x, int *dx, int *N, double *W, double* xh)
{
	int i,k;
	unsigned int *h=NULL;
	int *J1=(int*)malloc(sizeof(int)*(*N));

	if(*dx==1)
	{
		gsl_heapsort_index(J1, x, *N, sizeof(double), compare_double);
	}
	else
	{
		h=(unsigned int*)malloc(sizeof(unsigned int)*(*N));

		StateHilbertIndex(x, parPhi, dx, N , h);

		gsl_heapsort_index(J1, h, *N, sizeof(unsigned int), compare_uint);


		free(h);
		h=NULL;	
	}

	qsort(quasi, *N, (*dx+1)*sizeof(double), compare_double); 
	
	quasi_Resample(quasi, dx, N, W, J1, x, xh);

	free(J1);
	J1=NULL;	
	
}


void quasi_Resample(double *quasi, int *dx,  int *N, double *W, int *J1, double *x, double *xh)
{
	int i,j,k;
	double s;
	
    	s=0;
	j=0;

	for(i=0; i<*N; i++)
	{
		s+=W[J1[i]];

		while(quasi[(*dx+1)*j]<=s && j<*N)
		{
			for(k=0;k<*dx;k++)
			{
				xh[*dx*j+k]=x[*dx*J1[i]+k];
			}
			j++;

		}
		if(j==*N)
		{
			break;
		}
	     
	}
}

/*****************************************************************************************************************
			RESAMPLER ALGORITHMS FOR FORWARD STEP
******************************************************************************************************************/

int* ForHilbertResamplerQMC_32(double *quasi, int dimQuasi, double *parPhi, double *x, int *dx, int *N, int *Nb, double *W, double* xh)
{
	unsigned int *h=NULL;

	int *J1=(int*)malloc(sizeof(int)*(*N));

	if(*dx==1)
	{
		gsl_heapsort_index(J1, x, *N, sizeof(double), compare_double);
	}
	else
	{
		h=(unsigned int*)malloc(sizeof(unsigned int)*(*N));

		StateHilbertIndex(x, parPhi, dx, N , h);

		gsl_heapsort_index(J1, h, *N, sizeof(unsigned int), compare_uint);

		free(h);
		h=NULL;	
	}

	qsort(quasi, *Nb, dimQuasi*sizeof(double), compare_double); 
	
	quasi_ResampleFor(quasi, &dimQuasi, dx, N, Nb, W, J1, x, xh);

	return(J1);
		
}


int* ForHilbertResamplerQMC_64(double *quasi, int dimQuasi, double *parPhi, double *x, int *dx, int *N, int *Nb, double *W, double* xh)
{
	uint64_t *h=NULL;

	int *J1=(int*)malloc(sizeof(int)*(*N));

	if(*dx==1)
	{
		gsl_heapsort_index(J1, x, *N, sizeof(double), compare_double);
	}
	else
	{
		h=(uint64_t*)malloc(sizeof(uint64_t)*(*N));

		HilbertIndex(x, parPhi, dx, N , h);

		gsl_heapsort_index(J1, h, *N, sizeof(uint64_t), compare_uint64);

		free(h);
		h=NULL;	
	}

	qsort(quasi, *Nb, dimQuasi*sizeof(double), compare_double); 
	
	quasi_ResampleFor(quasi, &dimQuasi, dx, N, Nb, W, J1, x, xh);

	return(J1);
		
}




void quasi_ResampleFor(double *quasi, int *dimQuasi, int *dx,  int *N, int *Nb, double *W, int *J1, double *x, double *xh)
{
	int i,j,k;
	double s;
	
    	s=0;
	j=0;

	for(i=0; i<*N; i++)
	{
		s+=W[J1[i]];

		while(quasi[(*dimQuasi)*j]<=s && j<*Nb)
		{
			for(k=0;k<*dx;k++)
			{
				xh[*dx*j+k]=x[*dx*J1[i]+k];
			}

			j++;
		}
		if(j==*Nb)
		{
			break;
		}
	     
	}
}

/*****************************************************************************************************************
			RESAMPLER ALGORITHMS FOR  BACKWARD STEP
******************************************************************************************************************/

int* BackHilbertResamplerQMC_32(double *quasi, int dimQuasi, double *parPhi, double *x, int *dx, int *N, int *Nb, double *W, double* xh)
{
	unsigned int *h=NULL;

	int *J1=(int*)malloc(sizeof(int)*(*N));
	int *J2=(int*)malloc(sizeof(int)*(*Nb));

	if(*dx==1)
	{
		gsl_heapsort_index(J1, x, *N, sizeof(double), compare_double);
	}
	else
	{
		h=(unsigned int*)malloc(sizeof(unsigned int)*(*N));

		StateHilbertIndex(x, parPhi, dx, N , h);

		gsl_heapsort_index(J1, h, *N, sizeof(unsigned int), compare_uint);

		free(h);
		h=NULL;	
	}

	qsort(quasi, *Nb, dimQuasi*sizeof(double), compare_double); 
	
	quasi_ResampleBack(quasi, &dimQuasi, dx, N, Nb, W, J1, J2,x, xh);

	free(J1);
	J1=NULL;
	return(J2);
		
}


int* BackHilbertResamplerQMC_64(double *quasi, int dimQuasi, double *parPhi, double *x, int *dx, int *N, int *Nb, double *W, double* xh)
{
	int i;
	//FILE* fw=NULL;
	uint64_t *h=NULL;

	int *J1=(int*)malloc(sizeof(int)*(*N));
	int *J2=(int*)malloc(sizeof(int)*(*Nb));

	if(*dx==1)
	{
		gsl_heapsort_index(J1, x, *N, sizeof(double), compare_double);
	}
	else
	{
		h=(uint64_t*)malloc(sizeof(uint64_t)*(*N));

		HilbertIndex(x, parPhi, dx, N , h);

		gsl_heapsort_index(J1, h, *N, sizeof(uint64_t), compare_uint64);

		free(h);
		h=NULL;	
	}

	qsort(quasi, *Nb, dimQuasi*sizeof(double), compare_double); 
	
	quasi_ResampleBack(quasi, &dimQuasi, dx, N, Nb, W, J1, J2, x, xh);

	free(J1);
	J1=NULL;
	return(J2);
		
}






void quasi_ResampleBack(double *quasi, int *dimQuasi, int *dx,  int *N, int *Nb, double *W, int *J1, int *J2, double *x, double *xh)
{
	int i,j,k;
	double s;
	
    	s=0;
	j=0;

	for(i=0; i<*N; i++)
	{
		s+=W[J1[i]];

		while(quasi[(*dimQuasi)*j]<=s && j<*Nb)
		{
			for(k=0;k<*dx;k++)
			{
				xh[*dx*j+k]=x[*dx*J1[i]+k];
			}
			J2[j]=J1[i];
			j++;
		}
		if(j==*Nb)
		{
			break;
		}
	     
	}
}

	









		
	

