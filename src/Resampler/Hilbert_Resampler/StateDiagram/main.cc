/* copyright J.K.Lawder 2002 */
#include <stdio.h>
#include <stdlib.h>
#include "gendefs.h"
#include "gentable.h"
#include "state.h"

/*==========================================================================*/
/*                            main			                                */
/*==========================================================================*/
int main (void)
{
	int *X1index;
	ROW *Row;
	FILE *fp1, *fp2, *fp3;
 	state *head, *tail;
	char filename[50];

	sprintf (filename, "gen_tab_D.txt", (int)DIM);
	fp1 = fopen(filename, "w");
	sprintf (filename, "sdiag1_D.txt", (int)DIM);
	fp2 = fopen(filename, "w");
	sprintf (filename, "sdiag2_D.txt", (int)DIM);
	fp3 = fopen(filename, "w");

	Row     = (ROW *)calloc(NUMROWS, sizeof(ROW));
	X1index = (int *)calloc(NUMROWS, sizeof(int));

	createtable(Row, X1index);

	printtable(Row, fp1);

	if ( DIM < 8 )
	{
		table2states (Row, X1index, &head, &tail);

		processlist(head, &tail);

		outputSdiagram (head, fp2);

		output_sorted_state_diagram (head, fp3);

		outputSmatrices (head, fp1);
   	}
	else
	{
		printf ("DIM is too large for generating state diagrams\n");
	}

	fclose(fp1);
	fclose(fp2);
 	fclose(fp3);

	return 0;
}
