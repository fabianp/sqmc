/* copyright J.K.Lawder 2002 */
/*
 * Number of dimensions
 */
#ifndef DIM
#define	DIM		4
#endif
/*
 * Number of rows in state diagram generator table
 * (equals number of points on a first order curve
 */
#define	NUMROWS		(1 << DIM)

typedef unsigned char ONE_BYTE;
typedef	unsigned short TWO_BYTES;

/*
Permutation matrix:
PMR holds matrix as an array of rows:
	the top (used) bit is column 0.
	(bottom DIM bits of each row are used)
PMRsign holds the sign of the 1 in any row:
	the top (used) bit is row 0.
*/
typedef struct {
	unsigned int PMR[DIM];
	char         PMRsign[DIM];
} Pmatrix;

/* a row in the state diagram generator table */
typedef struct {
	unsigned int Y, X1, X2[2], dY;
	Pmatrix      PM;
} ROW;

/* a node in the state diagram / list */
typedef struct state {
	int           stateno;
	Pmatrix       PM;
	unsigned int  *X1;
	int           *nextstate;
	struct state  *next;
} state;
