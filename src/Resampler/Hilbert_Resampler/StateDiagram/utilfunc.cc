/* copyright J.K.Lawder 2002 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gendefs.h"

/*==========================================================================*/
/*                            int2bins		                                */
/*==========================================================================*/
/* takes a number and represents it as a binary string

   NB you can't call this function more than once in a single
   printf() statement otherwise you get duff results! */

char* int2bins(TWO_BYTES num, int bits)
{
	int i;
	static char bin[33];

	for (i = 0; i < (int)(sizeof bin); i++)
		bin[i] = '\0';

	if (bits > 32)
	{
		printf("error in int2bins()\n");
		exit(1);
	}

	for (i = 0; i < bits ; i++)
	{
		if (num % 2) bin[bits-1-i] = '1'; else bin[bits-1-i] = '0';
		num = num /2;
	}
	return bin;
}