/* copyright J.K.Lawder 2002 */

What this program does
----------------------
Generates state diagrams that encapsulate the Hilbert curve and state diagram
generator tables used to construct state diagrams, of the form described by
Theodore Bially (see references below).

State diagrams grow exponentially in size with the number of dimensions and,
for practical purposes, can be used in up to about 8 dimensions.

Program output
--------------

The program outputs 3 text files as follows (the 'n' in 'nD' is replaced by
the number of dimensions for which the program is run - see 'How to compile
it'):

gen_tab_nD.txt contains the state diagram generator table and the
transformation matrices for all states encapsulating how any state differs
from the state defined by the generator table itself.

sdiag1_nD.txt contains the state diagram expressed as a C structure and aiding
mapping calculations from the coordinates of a point to its Hilbert curve
sequence number.

sdiag2_nD.txt is similar to sdiag1_nD.txt but aids mapping in the inverse
direction.

How to compile it
-----------------
1. Determine the number of dimensions you want a state diagram for and either
a) edit gendefs.h and #define the macro 'DIM' accordingly or
b) edit Makefile so that a value in the form of '-DDIM=n' is assigned to
the variable 'DEFINES', where 'n' is the number of dimensions required. This
takes precedence over a).

2. Determine which compiler you want to use and assign it to 'COMPILER' in
Makefile.

3. Your choice of compiler may require changing the extensions of the files
named *.cc. In this case, modify the references to these within the makefile.

3. Run 'make Makefile' in the normal way to generate the target.

Files Required
--------------

gendefs.h
gentable.h
graycode.h
pmatrix.h
state.h
utilfunc.h
gentable.cc
graycode.cc
pmatrix.cc
state.cc
utilfunc.cc
main.cc
Makefile
readme.txt (this file)

How to use a state diagram
--------------------------

See example.cc

References
----------

1. Theodore Bially. Space-filling curves: Their generation and their
application to bandwidth reduction. IEEE Transactions on Information Theory,
IT-15(6):658-664, Nov 1969.

2. J.K. Lawder. The Application of Space-Filling Durves to the Storage and
Retrieval of Multi-dimensional Data. Ph.D. thesis, Birkbeck College,
University of London, 2000.

3. J.K. Lawder and P.J.H. King. Using state diagrams for Hilbert curve
mappings. International Journal of Computer Mathematics, Vol 78(3):327-342,
2001. (Formerly Birkbeck College Research Report BBKCS-00-02 or JL2/00, August
2000).