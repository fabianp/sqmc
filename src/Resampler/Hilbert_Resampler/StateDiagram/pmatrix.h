/* copyright J.K.Lawder 2002 */
void PMxPM (Pmatrix *, Pmatrix *, Pmatrix *);
unsigned int PMxNUM (Pmatrix *, unsigned int);
void setupPmatrix(ROW *, int);
void initmatrix(Pmatrix *);
int comparematrices(Pmatrix *, Pmatrix *);