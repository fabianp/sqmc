/* copyright J.K.Lawder 2002 */
#include <stdio.h>
#include <stdlib.h>
#include "gendefs.h"
#include "utilfunc.h"
#include "graycode.h"
#include "pmatrix.h"


/*==========================================================================*/
/*                            createtable	                                */
/*==========================================================================*/
/*
A ROW is a row from Bially's table (fig. 5).
gcar is an array for temporarily holding Gray Codes.
X1index is a way of getting at rows in X1 order such that X1index[X1] = Y:
note that the rows are ordered on Y.
*/
int createtable (ROW *Row, int *X1index)
{
	int	i, setbit = DIM - 1;

	unsigned int gcar[NUMROWS] = {0}, X2gcar[2 * NUMROWS] = {0};

	GCF (DIM, 0, NUMROWS-1, gcar);

	X2GCF (DIM, 0, 2*NUMROWS-1, X2gcar);

	/* setbit is the bit position in the last X1 entry that's 1
	   this is the bit position where the user could be asked to enter
	   an alternative setbit value */

	/* fill in columns Y, X1, X2, dY and  */
	for (i = 0; i < NUMROWS; i++)
	{
		Row[i].Y  = i;
		Row[i].X1 = gcar[i];
		Row[i].X2[0] = X2gcar[2*i];
		Row[i].X2[1] = X2gcar[2*i+1];
		/* at this stage all the topbits in X1, and X2 could be moved
		   to the setbit position had the user entered an alternative
		   to the hard coded one above:*/

		Row[i].dY = Row[i].X2[0] ^ Row[i].X2[1];

		setupPmatrix(&Row[i], setbit);
	}

	/* setup index on X1 */
	for (i = 0; i < NUMROWS; i++)
		X1index[Row[i].X1] = i;

	return 0;
}

/*==========================================================================*/
/*                            printtable	                                */
/*==========================================================================*/
/* output the state diagram generator table */
int printtable(ROW *Row, FILE *fp)
{
	int i, row;

	fprintf(fp, "\tSTATE DIAGRAM GENERATOR TABLE FOR %i DIMENSIONS \n", DIM);
	fprintf(fp, "\tHILBERT CURVE\n\n");

	/* output in binary format */
	for (i = 0; i < NUMROWS; i++)
	{
		/* first row */
		fprintf(fp,"%-4s%-10s", "", int2bins((TWO_BYTES)Row[i].Y, DIM));
		fprintf(fp,"%-10s",         int2bins((TWO_BYTES)Row[i].X1, DIM));
		fprintf(fp,"%-10s",         int2bins((TWO_BYTES)Row[i].X2[0], DIM));
		fprintf(fp,"%-10s",         int2bins((TWO_BYTES)Row[i].dY, DIM));
		fprintf(fp,"%-5c",          Row[i].PM.PMRsign[0]);
		fprintf(fp,"%-10s\n",       int2bins((TWO_BYTES)Row[i].PM.PMR[0], DIM));

		/* second row */
		fprintf(fp,"%24s%-10s%10s",
				"",
				int2bins((TWO_BYTES)Row[i].X2[1], DIM),
				"");
		fprintf(fp,"%-5c",          Row[i].PM.PMRsign[1]);
		fprintf(fp,"%-10s\n",         int2bins((TWO_BYTES)Row[i].PM.PMR[1], DIM));

		/* remaining row(s) */
		for (row = 2; row < DIM; row++)
		{
			fprintf(fp,"%44s", "");
			fprintf(fp,"%-5c", Row[i].PM.PMRsign[row]);
			fprintf(fp,"%-10s\n", int2bins((TWO_BYTES)Row[i].PM.PMR[row], DIM));

		}
		fprintf(fp,"\n");
	}

	fflush(fp);

	return 0;
}
