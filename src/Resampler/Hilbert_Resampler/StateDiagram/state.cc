#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "gendefs.h"
#include "pmatrix.h"
#include "utilfunc.h"

/* NOTES:

typedef unsigned int U_int;

#define POINTS (1 << DIM)

typedef struct Node{
    U_int state[POINTS],
          value[POINTS];
} SDNode;
*/

/*==========================================================================*/
/*                            newstate		                                */
/*==========================================================================*/
static state* newstate (void)
{
	state *P;

	assert ((P     = (state *)malloc(sizeof(state))) != NULL);
	P->stateno     = 0;
	assert ((P->X1 = (unsigned int *)calloc(1 << DIM, sizeof(unsigned int))) != NULL);
	assert ((P->nextstate = (int *)calloc(1 << DIM, sizeof(int))) != NULL);
	P->next        = NULL;

	return P;
}

/*==========================================================================*/
/*                            inlist		                                */
/*==========================================================================*/
/* Checks to see if a state with a particular Pmatrix is already in
   the list.
   Returns the state's stateno if it is otherwise -1. */
static int inlist (state *P, Pmatrix *PM)
{
	if (P == NULL)
		return -1;
	if (comparematrices(&P->PM, PM))
		return P->stateno;
	return inlist(P->next, PM);
}

/*==========================================================================*/
/*                            addstate		                                */
/*==========================================================================*/
/* Adds a state to the end of the list and returns a pointer to it. */
static state* addstate (state *tail, Pmatrix *PM)
{
	int i;
	state *P;

	P = newstate();
	P->stateno = tail->stateno + 1;
	tail->next = P;
	for (i = 0; i < DIM; i++)
	{
		P->PM.PMR[i]     = PM->PMR[i];
		P->PM.PMRsign[i] = PM->PMRsign[i];
	}

	return P;
}

/* copyright J.K.Lawder 2002 */
/*==========================================================================*/
/*                            table2states	                                */
/*==========================================================================*/
/* Puts the first state in the list and all of its next states
   Remember X1index[i] contains an output value = a Row number;
   it is ordered on input (coords) */
void table2states (ROW *Row, int *X1index, state **head, state **tail)
{
	int i, nextstate;

	*head = *tail = newstate();
	(*head)->stateno = 0;
	initmatrix(&(*head)->PM);
	for (i = 0; i < NUMROWS; i++)
		(*head)->X1[i] = X1index[i];
	/* add the table's next states to the list */
	for (i = 0; i < NUMROWS; i++)
		if ((nextstate = inlist(*head, &Row[X1index[i]].PM)) == -1)
		{
			*tail = addstate(*tail, &Row[X1index[i]].PM);
			(*head)->nextstate[i] = (*tail)->stateno;
		}
		else
			(*head)->nextstate[i] = nextstate;
}

/*==========================================================================*/
/*                            processlist	                                */
/*==========================================================================*/
/* Traverses the state list filling in the X1 and nextstate arrays
   and adding new states as required */
void processlist(state *head, state **tail)
{
	int nextstate;
	unsigned int i, tempX1;
	Pmatrix newPM;
	state *P, *temp;

	if (head == NULL)
		exit(printf("Error in processlist()\n"));

	P = head->next;

	for ( ; P != NULL; P = P->next)
	{
		for (i = 0; i < NUMROWS; i++)
		{
			/* transform i to state 0 equivalent */
			tempX1 = PMxNUM(&P->PM, i);
			P->X1[i] = head->X1[tempX1];
			/* find tempX1's state 0 next state */
			for (temp = head; temp->stateno != head->nextstate[tempX1];
				 temp = temp->next);
			/* calculate nextstate matrix for P->X1[i] */
			PMxPM(&temp->PM, &P->PM, &newPM);
			/* see if P->X1[i]'s next state is already in the list */
			if ((nextstate = inlist(head, &newPM)) == -1)
			{
				*tail = addstate(*tail, &newPM);
				P->nextstate[i] = (*tail)->stateno;
			}
			else
				P->nextstate[i] = nextstate;
		}
	}
}

/*==========================================================================*/
/*                            outputSdiagram                                */
/*==========================================================================*/
/* Creates a C structure encapsulating a state diagram - meant for mapping
   from the coordinates of a point to its hilbert curve sequence number.
   See definition of an SDNode in comment at head of this file */
void outputSdiagram (state *head, FILE * fp)
{
	int i;

	fprintf(fp, "\/*\n\tSTATE DIAGRAM FOR %i DIMENSIONS \n\n", DIM);
	fprintf(fp, "\tEach pair of rows corresponds to a state.\n"
				"\tOrdered by coordinates (X1 values):\n"
		    	"\tcoordinates are implied,\n"
		    	"\tfirst row of a pair of rows contains 'next state',\n"
		    	"\tsecond row contains hilbert curve sequence\n"
				"\tnumbers (Y values).\n*/\n\n");
	fprintf(fp, "const SDNode g_curveND[] =\n{\n");

	fflush(fp);

	for ( ; head != NULL; head = head->next)
	{
		fprintf(fp, "\t");
		for (i = 0; i < NUMROWS; i++)
			fprintf(fp, "%4i,", head->nextstate[i]);
		fprintf(fp, "\n\t");
		for (i = 0; i < NUMROWS; i++)
			fprintf(fp, "%4i,", head->X1[i]);
		fprintf(fp, "\n");
	}
	fflush(fp);

	fprintf(fp, "};\n\n");
}

/*==========================================================================*/
/*                            outputSmatrices                               */
/*==========================================================================*/
/* Outputs the transformation matrix that defines a state, for all states */
void outputSmatrices (state *head, FILE * fp)
{
	int row;

	fprintf(fp, "\n\tSTATE DIAGRAM MATRICS FOR %i DIMENSIONS \n\n", DIM);
	fflush(fp);

	for ( ; head != NULL; head = head->next)
	{
		fprintf(fp, "\tSTATE NO. : %i\n", head->stateno);
		for (row = 0; row < DIM; row++)
		{
			fprintf(fp,"\t%-5c",    head->PM.PMRsign[row]);
			fprintf(fp,"\t%-10s\n",   int2bins((TWO_BYTES)head->PM.PMR[row], DIM));
		}
		fprintf(fp,"\n");
	}
	fflush(fp);
}

/* THE NEXT LOT OF CODE SORTS THE INDIVIDUAL STATES SO ENTRIES WITHIN THEM ARE
   IN HILBERT CODE ORDER - AND OUTPUTS THE RESULT (IE. HCODES ARE IMPLIED AND
   COORDS ARE SPECIFIED) */

typedef struct {
	int state, coord, hcode;
} BLOB;

typedef struct {
	BLOB	blob[NUMROWS];
} NODE;

/*==========================================================================*/
/*                            sort_fn		                                */
/*==========================================================================*/
int sort_fn(const void *a, const void *b)
{
	const BLOB* v1 = (BLOB *)a;
	const BLOB* v2 = (BLOB *)b;

	return (v1->hcode - v2->hcode);
}

/*==========================================================================*/
/*                            output_sorted_state_diagram                   */
/*==========================================================================*/
/* inverse of outputSdiagram()
   Creates a C structure encapsulating a state diagram - meant for mapping
   from a hilbert curve sequence number to the coordinates of a point.
   See definition of an SDNode in comment at head of this file */
void output_sorted_state_diagram(state *head, FILE * fp)
{
	state	*temp = head;
	int	i, j, scount;
	NODE	*nptr;

	fprintf(fp, "\n\/*\tSTATE DIAGRAM FOR %i DIMENSIONS \n\n", DIM);
	fprintf(fp, "\tEach pair of rows corresponds to a state.\n"
				"\tOrdered by hilbert curve sequence numbers (Y values):\n"
		    	"\thilbert curve sequence numbers are implied,\n"
		    	"\tfirst row of a pair of rows contains 'next state',\n"
		    	"\tsecond row contains coordinates (X1 values).\n*/\\n\n");
	fprintf(fp, "const SDNode g2_curveND[] =\n{\n");

	fflush(fp);
 	/* count the number of states */
	for (scount = 0; temp != NULL; temp = temp->next, scount++);

	nptr = (NODE *)calloc(scount, sizeof(NODE));

	for (j = 0; head != NULL; head = head->next, j++)
	{
		for (i = 0; i < NUMROWS; i++)
		{
			nptr[j].blob[i].coord = i;
			nptr[j].blob[i].state = head->nextstate[i];
			nptr[j].blob[i].hcode = head->X1[i];
		}
		qsort(&nptr[j], NUMROWS, sizeof (BLOB), sort_fn);
	}

	for (j = 0; j < scount; j++)
	{
		fprintf(fp, "\t");
		for (i = 0; i < NUMROWS; i++)
			fprintf(fp, "%4i,", nptr[j].blob[i].state);
		fprintf(fp, "\n\t");
		for (i = 0; i < NUMROWS; i++)
			fprintf(fp, "%4i,", nptr[j].blob[i].coord);
		fprintf(fp, "\n");
	}

	fprintf(fp, "};\n");

	fflush(fp);
}
