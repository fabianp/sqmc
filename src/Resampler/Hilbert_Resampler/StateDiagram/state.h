/* copyright J.K.Lawder 2002 */
void table2states (ROW *, int *, state **, state **);
void processlist (state *, state **);
void outputSdiagram (state *, FILE *);
void outputSmatrices (state *head, FILE * fp);
void output_sorted_state_diagram(state *, FILE *);