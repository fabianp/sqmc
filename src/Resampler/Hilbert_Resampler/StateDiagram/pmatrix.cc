/* copyright J.K.Lawder 2002 */
#include "gendefs.h"
#include "utilfunc.h"


/*==========================================================================*/
/*                            PMxPM			                                */
/*==========================================================================*/
/* multiplies two permutation matrices */
void PMxPM (Pmatrix *PM1, Pmatrix *PM2, Pmatrix *newPM)
{
	int i, col, row;

	/* initialize the new matrix */
	for (i = 0; i < DIM; i++)
	{
		newPM->PMR[i]     = 0;
		newPM->PMRsign[i] = '+';
	}

	/* do the multiplication */
	for (col = 0; col < DIM; col++)
	{
		for (row = 0; row < DIM; row++)
			if (PM1->PMR[row] & 1 << DIM - 1 - col)
				break;
		newPM->PMR[row] = PM2->PMR[col];
		if (PM1->PMRsign[row] != PM2->PMRsign[col])
			newPM->PMRsign[row] = '-';
	}
}


/*==========================================================================*/
/*                            PMxNUM		                                */
/*==========================================================================*/
/* applies a permutation matrix to a number to transform it */
unsigned int PMxNUM (Pmatrix *PM, unsigned int orignum)
{
	int row;
	unsigned int newnum = 0;
	/* maskbit is used to extract the bit value in one 'column'
	mask = value of a num that contains only the maskbit value; other bits = 0 */

	/* first invert the bits in orignum as required */
	for (row = 0; row < DIM; row++)
	{
		if (PM->PMRsign[row] == '+')
			continue; /* nothing to do */

		if (orignum &   PM->PMR[row])
			orignum &= ~PM->PMR[row]; /* change a 1 bit to 0 */
		else
			orignum |=  PM->PMR[row];  /* change a 0 bit to 1 */
	}

	/* next move the 'columns' around (or not); put the result in newnum;
	   there's no need to move 0's since newnum initialized to 0's */
	for (row = 0; row < DIM; row++)
		if (1 << DIM - 1 - row == (int)(PM->PMR[row]))
			newnum |= (orignum & PM->PMR[row]);
		else
			if (orignum & PM->PMR[row])
				newnum |= (1 << DIM - 1 - row);

	return newnum;
}

/*==========================================================================*/
/*                            initmatrix	                                */
/*==========================================================================*/
/* setup the identity matrix */
void initmatrix(Pmatrix *PM)
{
	int i;

	for (i = 0; i < DIM; i++)
	{
		PM->PMR[i]     = 1 << DIM - 1 - i;
		PM->PMRsign[i] = '+';
	}
}

/*==========================================================================*/
/*                            setupPmatrix	                                */
/*==========================================================================*/
/* setup Pmatrix for the state diagram generator table
   setbit is the (decimal) number of the bit that's 1 in last X1 in table */
void setupPmatrix(ROW *Row, int setbit)
{
	int i, j, row, dY;

	/* initialize Pmatrix as identity matrix */
	initmatrix(&Row->PM);

	/* find the (decimal) bit position of the bit that's 1 in Row->dY */
	for (i = DIM - 1, j = 1 << DIM - 1; !(j & Row->dY); i--, j >>= 1);

	dY = Row->dY;
	for (row = 0; row < DIM; row++)
	{
		Row->PM.PMR[row] = dY;
		dY >>= 1;
		if (dY == 0)
			dY = 1 << DIM - 1;
	}

	/* set Row->PM.PMRsign[] as appropriate */
	for (row = 0; row < DIM; row++)
		if (Row->X2[0] & Row->PM.PMR[row])
			Row->PM.PMRsign[row] = '-';
}

/*==========================================================================*/
/*                            comparematrices                               */
/*==========================================================================*/
/* compares 2 matrices to see if they are the same */
int comparematrices(Pmatrix *PM1, Pmatrix *PM2)
{
	int i;

	for (i = 0; i < DIM; i++)
		if (PM1->PMR[i]     != PM2->PMR[i] ||
			PM1->PMRsign[i] != PM2->PMRsign[i])
			return 0; /* matrices are not the same */
	return 1;         /* matrices are the same */
}