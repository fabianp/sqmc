/* copyright J.K.Lawder 2002 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gendefs.h"


static void GCR(int n, int a, int b, unsigned int gcar[]);

/*==========================================================================*/
/*                            GCF			                                */
/*==========================================================================*/
/*
calculates gray codes in 'forward' order
'a' and 'b' hold the range of indeces (lowest to highest, inclusive) of gcar
elements which are to be handled by the function call.
initially, a = 0 and b = pow(2, n) - 1.
no. of required elements in gcar[] is pow(2,n)
*/
void GCF(int n, int a, int b, unsigned int gcar[])
{
	int i, j;

	if (n == 1)
	{
		gcar[b]++;
		return;
	}

	j = (b - a) / 2 + a;

	for (i = j + 1; i <= b; i++)
		gcar[i] += (int)pow(2, n - 1);

	GCF(n - 1, a, j, gcar);
	GCR(n - 1, j + 1, b, gcar);
}

/*==========================================================================*/
/*                            GCR			                                */
/*==========================================================================*/
/*
calculates gray codes in reverse order
static as only GCF() in this file calls it
*/
static void GCR(int n, int a, int b, unsigned int gcar[])
{
	void GCF(int n, int a, int b, unsigned int gcar[]);
	int i, j;

	if (n == 1)
	{
		gcar[a]++;
		return;
	}

	j = (b - a) / 2 + a;

	for (i = a; i <= j; i++)
		gcar[i] += (int)pow(2, n - 1);

	GCF(n - 1, a, j, gcar);
	GCR(n - 1, j + 1, b, gcar);
}

static void X2GCR(int n, int a, int b, unsigned int gcar[]);

/*==========================================================================*/
/*                            X2GCF			                                */
/*==========================================================================*/
/*
calculates entries for Bially's column X2 in a pseudo gray code manner.
'a' and 'b' hold the range of indeces (lowest to highest, inclusive) of gcar
elements which are to be handled by the function call.
initially, a = 0, b = 2 * pow(2, n) - 1 (or pow(2, n+1) -1)
no. of required elements in gcar[] is 2 * (pow(2,n))
*/
void X2GCF(int n, int a, int b, unsigned int gcar[])
{
	int i, j;

	if (n == 1)
	{
		gcar[a+1]++;
		gcar[b]++; /* b == a+3 */
		return;
	}

	j = (b - a) / 2 + a;

	X2GCF(n - 1, a, j, gcar);
	X2GCR(n - 1, j + 1, b, gcar);

	gcar[j] = gcar[j-1] + (int)pow(2, n - 1);
	gcar[j+1] = gcar[j+2];
	for (i = j + 2; i <= b; i++)
		gcar[i] += (int)pow(2, n - 1);
}

/*==========================================================================*/
/*                            X2GCR     	                                */
/*==========================================================================*/
/*
calculates X2 values in reverse order
static as only X2GCF() in this file calls it
*/
static void X2GCR(int n, int a, int b, unsigned int gcar[])
{
	int i, j;

	if (n == 1)
	{
		gcar[a]++;
		gcar[a+2]++;
		return;
	}

	j = (b - a) / 2 + a;

	X2GCF(n - 1, a, j, gcar);
	X2GCR(n - 1, j + 1, b, gcar);

	gcar[j] = gcar[j-1];
	gcar[j+1] = gcar[j+2] + (int)pow(2, n - 1);
	for (i = a; i < j; i++)
		gcar[i] += (int)pow(2, n - 1);
}