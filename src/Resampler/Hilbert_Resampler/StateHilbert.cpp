
#include"../../../include/Resampler/Hilbert_Resampler/StateHilbert.hpp"
#include "StateDiagram/sdiag1_D.txt"
#include <math.h>
#include <omp.h>


#define NUMBITS 32
#define WORDBITS 32


const U_int g_mask[] = {4, 2, 1};

/*Compute the Hilbert Indexes using state diagram */

void StateHilbertIndex(double *x, double* par, int *dx, int *N, unsigned int *h)
{
	int i,k, count;
	int u=floor(NUMBITS/(*dx));
	int dd=floor((u-1)*log(2)/log(10));
	double val1, val2, factor;
	double *work = new double[*N*(*dx)];

	for(i=0;i<*N;i++)
	{
		for(k=0;k<*dx;k++)
		{
			work[*dx*i+k]=-log(1+exp(-(x[*dx*i+k]-par[2*k])/(par[2*k+1]-par[2*k])));
			if(i==0 && k==0)
			{
				val1=work[*dx*i+k];
			}
			else if(work[*dx*i+k]>val1);
			{
				val1=work[*dx*i+k];
			}
		}
	}

	val2=exp(val1);
	count=0;
	while(static_cast<long>(val2) % 10 == 0)
	{
    		val2*= 10;
    		++count;
	}

	factor=(dd+count)*log(10);

	Point p1,p2;

	for(i=0;i<*N;i++)
	{
		for(k=0;k<*dx;k++)
		{
			p1.hcode[k]=(U_int)exp(work[*dx*i+k]+factor);
		}
		p2=H_encode(p1);
		h[i]=p2.hcode[0];
	}

	delete [] work;
	work=NULL;
}



/*============================================================================*/
/*                            H_encode  			      */
/*============================================================================*/
/* Converts a multiattribute point to a Hilbert code.
   Assumes that the values of p elements reside at the tops of their elements. */
Hcode H_encode(Point p)
{
	U_int mask = (U_int)1 << WORDBITS - 1, pval, element;
	Hcode h = {0};
	int i, j, state = 0;

	for (i = NUMBITS * DIM - DIM; i >=0; i -= DIM, mask >>= 1)
	{
		pval = 0;
		for (j = 0; j < DIM; j++) /* extract top bits from p */
			if (p.hcode[j] & mask)
				pval |= g_mask[j];
		/* add in DIM bits to hcode */
		element = i / WORDBITS;
		if (i % WORDBITS > WORDBITS - DIM)
		{
			h.hcode[element] |= g_curveND[state].value[pval] << i % WORDBITS;
			h.hcode[element + 1] |=
				g_curveND[state].value[pval] >> WORDBITS - i % WORDBITS;
		}
		else
			h.hcode[element] |=
				g_curveND[state].value[pval] << i - element * WORDBITS;

		state = g_curveND[state].state[pval];
	}
	return h;
}

