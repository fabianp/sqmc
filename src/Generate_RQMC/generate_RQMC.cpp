


#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <math.h>

using namespace std;

#include "../../include/Generate_RQMC/SamplePack/DigitalNetsBase2.h"
#include "../../include/Generate_RQMC/generate_RQMC.hpp"

#define MINU 1e-10

typedef unsigned int     uint;

bitvector rng32()
{
  return (bitvector)(drand48()*((double)BV_MAX+1.0));
}

extern "C"{

// interface for the constructor

DigitalNetGenerator *DigitalNetGenerator_Create(int *seq, int *d) 
{
	dgt gentype;

	if(*seq==1)
	{
		gentype=dgt_Sobol;
	}
	else if(*seq==2)
	{
		gentype=dgt_SpecialNiederreiter;
	}
	else if(*seq==3)
	{
		gentype=dgt_NiederreiterXing;
	}
    	return new DigitalNetGenerator(gentype, *d);
}

LinearScrambled * LinearScrambled_Create(int *seq, int *d) 
{
	dgt gentype;

	if(*seq==1)
	{
		gentype=dgt_Sobol;
	}
	else if(*seq==2)
	{
		gentype=dgt_SpecialNiederreiter;
	}
	else if(*seq==3)
	{
		gentype=dgt_NiederreiterXing;
	}
	else
	{
		gentype=dgt_ShiftNet;
	}
	
    	return new LinearScrambled(gentype, *d, rng32);
}
 

Scrambled * Scrambled_Create(int *seq, int *d, int *N) 
{
	dgt gentype;

	if(*seq==1)
	{
		gentype=dgt_Sobol;
	}
	else if(*seq==2)
	{
		gentype=dgt_SpecialNiederreiter;
	}
	else if(*seq==3)
	{
		gentype=dgt_NiederreiterXing;
	}
	else
	{
		gentype=dgt_ShiftNet;
	}
	
    	return new Scrambled(gentype, *d, *N+1, rng32);
}

// interface for the destructor

void DigitalNetGenerator_Destroy(DigitalNetGenerator* os ) 
{
    delete os;
}

void LinearScrambled_Destroy(LinearScrambled* os ) 
{
    delete os;
}

void Scrambled_Destroy(Scrambled* os ) 
{
    delete os;
}

// interface to randomize points set

void LinearScrambled_Randomize(LinearScrambled *os) 
{
    os->Randomize();
}


void Scrambled_Randomize(Scrambled *os) 
{
    os->Randomize();
}


void DigitalNetGenerator_GetPoints(DigitalNetGenerator *os, int* d, int* N, double *x) 
{
	int i,j;
	
	for (i = 0; i < *N; i++)
	{
	  	for (j = 0; j < *d; j++)
		{
	    		x[i*(*d)+j] =fmax((*os)[j],MINU);
		}
		++(*os);
	}

}

void LinearScrambled_GetPoints(LinearScrambled *os, int* d, int* N, double *x) 
{
	int i,j;
	
	++(*os);
	for (i = 0; i < *N; i++)
	{
	  	for (j = 0; j < *d; j++)
		{
	    		x[i*(*d)+j] =(*os)[j];
		}
		++(*os);
	}

}



void Scrambled_GetPoints(Scrambled *os, int* d, int* N, double *x) 
{
	int i,j;
	
	for (i = 0; i < *N; i++)
	{
	  	for (j = 0; j < *d; j++)
		{
	    		x[i*(*d)+j] = (*os)[j];
			if(x[i*(*d)+j]==0)
			{
				x[i*(*d)+j]=MINU;
			}
		}
		++(*os);
	}

}


};




