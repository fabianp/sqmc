


#ifdef _cplusplus 
extern "C" {
	DigitalNetGenerator *DigitalNetGenerator_Create(int*, int*);
	void DigitalNetGenerator_Destroy(DigitalNetGenerator*); 
	void DigitalNetGenerator_GetPoints(DigitalNetGenerator*, int*, int*, double*); 

    	LinearScrambled*  LinearScrambled_Create(int*, int*);
    	void LinearScrambled_Randomize(LinearScrambled*);
	void LinearScrambled_GetPoints(LinearScrambled*,int*, int*, double*);
	void LinearScrambled_Destroy(LinearScrambled*);

	Scrambled*  Scrambled_Create(int*, int*, int*);
    	void Scrambled_Randomize(Scrambled*);
	void Scrambled_GetPoints(Scrambled*,int*, int*, double*);
	void Scrambled_Destroy(Scrambled*);
}
 #endif
