
#define	DIM 4	

#ifdef __cplusplus
    #include <iostream>
    using namespace std;
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define POINTS (1 << DIM)

typedef unsigned int U_int;


typedef struct SDNode{
    U_int state[POINTS],
          value[POINTS];
} SDNode;

typedef struct Hcode{
	U_int	hcode[DIM];
}Hcode;

typedef Hcode Point;




void StateHilbertIndex(double*, double*, int*, int*, unsigned int*);

Hcode H_encode(Point);

#ifdef __cplusplus
    }
#endif
