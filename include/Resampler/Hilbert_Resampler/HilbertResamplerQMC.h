
	
#include <stdlib.h>
#include <gsl/gsl_heapsort.h> 
  		
#include "HilbertCode.hpp"
#include "StateHilbert.hpp"

#include "../../functions.h"


void HilbertResamplerQMC_32(double*,  double*, double*, int*, int*, double*, double*);
void HilbertResamplerQMC_64(double*,  double*, double*, int*, int*, double*, double*);

int *ForHilbertResamplerQMC_32(double*, int, double*, double*, int*, int*, int*, double*, double*);
int *ForHilbertResamplerQMC_64(double*, int,  double*, double*, int*, int*, int*, double*, double*);
	
int *BackHilbertResamplerQMC_32(double*, int, double*, double*, int*, int*, int*, double*, double*);
int *BackHilbertResamplerQMC_64(double*, int,  double*, double*, int*, int*, int*, double*, double*);


void quasi_Resample(double*, int*,int*, double*, int *, double*, double*);
void quasi_ResampleFor(double*, int *, int*, int*, int*, double*, int *,  double*, double*);

void quasi_ResampleBack(double*, int *, int*, int*, int*, double*, int *,int*, double*, double*);



