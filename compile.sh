##MULTIVARIATE SV MODEL


mkdir fo

## Sample Pack

g++ -I/usr/share/R/include      	-fpic  -O3 -pipe  -g -c src/Generate_RQMC/SamplePack/DigitalNetsBase2.C -o fo/DigitalNetsBase2.o

g++ -I/usr/share/R/include      	-fpic  -O3 -pipe  -g -c src/Generate_RQMC/generate_RQMC.cpp -o fo/generate_RQMC.o


## Some useful functions

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/functions.c -o fo/functions.o

g++ -I/usr/share/R/include      	-fpic  -O3 -pipe  -g -c src/functionsCC.cpp -o fo/functionsCC.o

## Hilbert resampling

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Resampler/Hilbert_Resampler/HilbertResamplerQMC.c -o fo/HilbertResamplerQMC.o

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Resampler/Resampler.c -o fo/Resampler.o

g++ -I/usr/share/R/include      -fopenmp -fpic  -O3 -pipe  -g -c src/Resampler/Hilbert_Resampler/HilbertCode.cpp -o fo/HilbertCode.o

g++ -I/usr/share/R/include      -fopenmp -fpic  -O3 -pipe  -g -c src/Resampler/Hilbert_Resampler/StateHilbert.cpp -o fo/StateHilbert.o


## Particles filters algorithms

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/SMC.c -o fo/SMC.o

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/SMC_Back.c -o fo/SMC_Back.o

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/SQMC.c -o fo/SQMC.o

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/SQMC_Back.c -o fo/SQMC_Back.o

## Models

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Models/LinearGaussian_Model/LG_Model.c -o fo/LG_Model.o


gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Models/SV_Model/SV_Model.c -o fo/SV_Model.o
gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Models/SV_Model/SV_SQMC.c -o fo/SV_SQMC.o

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Models/Neuro_Model/Neuro_Model.c -o fo/Neuro_Model.o
gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Models/Neuro_Model/Neuro_SQMC.c -o fo/Neuro_SQMC.o

gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Models/Univariate_Model/Univ_Model.c -o fo/Univ_Model.o
gcc -std=gnu99 -I/usr/share/R/include   -fpic  -O3 -pipe  -g -c src/Models/Univariate_Model/Univ_SQMC.c -o fo/Univ_SQMC.o


##Shared objects 

g++ -shared -o src/Models/SV_Model/SV_SQMC.so  fo/SV_SQMC.o fo/SV_Model.o fo/LG_Model.o fo/HilbertResamplerQMC.o fo/Resampler.o fo/functions.o  fo/functionsCC.o fo/HilbertCode.o fo/StateHilbert.o  fo/SMC.o fo/SMC_Back.o fo/SQMC.o fo/SQMC_Back.o fo/generate_RQMC.o fo/DigitalNetsBase2.o -lgsl -lgslcblas -fopenmp  -L/usr/lib/R/lib -lR

g++ -shared -o src/Models/Neuro_Model/Neuro_SQMC.so  fo/Neuro_SQMC.o fo/Neuro_Model.o fo/LG_Model.o fo/HilbertResamplerQMC.o fo/Resampler.o fo/functions.o  fo/functionsCC.o fo/HilbertCode.o fo/StateHilbert.o fo/SMC.o fo/SMC_Back.o fo/SQMC.o fo/SQMC_Back.o fo/generate_RQMC.o fo/DigitalNetsBase2.o -lgsl -lgslcblas -fopenmp  -L/usr/lib/R/lib -lR


g++ -shared -o src/Models/Univariate_Model/Univ_SQMC.so  fo/Univ_SQMC.o fo/Univ_Model.o fo/HilbertResamplerQMC.o fo/Resampler.o fo/functions.o  fo/functionsCC.o fo/HilbertCode.o fo/StateHilbert.o fo/SMC.o fo/SMC_Back.o fo/SQMC.o fo/SQMC_Back.o fo/generate_RQMC.o fo/DigitalNetsBase2.o  -lgsl -lgslcblas -fopenmp -L/usr/lib/R/lib -lR

rm -r fo















