# define the flags
CXXFLAGS = -fpic  -O3 -pipe  -g -I/usr/share/R/include -I/usr/local/include $(EXTRA_FLAGS)
CFLAGS = -std=gnu99 -I/usr/share/R/include -I/usr/local/include -fpic  -O3 -pipe  -g $(EXTRA_FLAGS)
LINKFLAGS = -lgsl -lgslcblas -fopenmp  \
    -L/usr/lib/R/lib -lR $(EXTRA_FLAGS)

SV_SQMC_OBJS = \
	src/Models/SV_Model/SV_Model.o \
	src/Models/SV_Model/SV_SQMC.o \
	src/Models/LinearGaussian_Model/LG_Model.o \
	src/Resampler/Hilbert_Resampler/HilbertResamplerQMC.o \
	src/Resampler/Resampler.o \
	src/functions.o \
	src/functionsCC.o \
	src/Resampler/Hilbert_Resampler/HilbertCode.o \
	src/Resampler/Hilbert_Resampler/StateHilbert.o \
	src/SQMC.o \
	src/SQMC_Back.o \
	src/SMC.o \
	src/SMC_Back.o \
	src/Generate_RQMC/SamplePack/DigitalNetsBase2.o \
	src/Generate_RQMC/generate_RQMC.o \

Neuro_SQMC_OBJS = $(SV_SQMC_OBJS) \
	src/Models/Neuro_Model/Neuro_Model.o \
	src/Models/Neuro_Model/Neuro_SQMC.o \

Univ_SQMC_OBJS = $(SV_SQMC_OBJS) \
		src/Models/Univariate_Model/Univ_Model.o \
		src/Models/Univariate_Model/Univ_SQMC.o

SV_SQMC = src/Models/SV_Model/SV_SQMC.so
Neuro_SQMC = src/Models/Neuro_Model/Neuro_SQMC.so
Univ_SQMC = src/Models/Univariate_Model/Univ_SQMC.so

all: $(SV_SQMC) $(Univ_SQMC) $(Neuro_SQMC) 

$(SV_SQMC): $(SV_SQMC_OBJS)
	$(CXX) -shared -o $(SV_SQMC) \
		$(SV_SQMC_OBJS) $(LINKFLAGS)


$(Neuro_SQMC): $(Neuro_SQMC_OBJS)
	$(CXX) -shared -o $(Neuro_SQMC) \
		$(Neuro_SQMC_OBJS) $(LINKFLAGS)

$(Univ_SQMC): $(Univ_SQMC_OBJS)
	$(CXX) -shared -o $@ \
		$(Univ_SQMC_OBJS) $(LINKFLAGS)

%.o : %.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@

%.o : %.c
	$(CC) -c $(CFLAGS) $< -o $@

lib: $(LIB_OBJS)

clean:
	rm $(SV_SQMC_OBJS) $(SV_SQMC) $(Neuro_SQMC) $(Univ_SQMC)

